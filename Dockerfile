FROM python:2.7.12

MAINTAINER Tanu Malik <scidataspace@gmail.com>

LABEL "EarthCube Project"="GeoTrust" "Tutorial"="First"

USER root

RUN apt-get install git
WORKDIR /home/ubuntu/
RUN git clone https://tanum@bitbucket.org/geotrust/sciunit-cli.git /home/ubuntu/sciunit-cli

WORKDIR /home/ubuntu/sciunit-cli/sciunit/client
#RUN cd sciunit-cli/sciunit
#RUN cd client/
RUN python setup.py install
WORKDIR /home/ubuntu/sciunit-cli
RUN python setup.py install globus
RUN python setup.py install 

ENV FIRST Bakinam-SmallWorkflow

RUN mkdir -p /home/ubuntu/Hydrology
#RUN mkdir -p /home/ubuntu/SolidEarth
#RUN mkdir -p /homei/ubuntu/Spacescience
 
COPY $FIRST /home/ubuntu/Hydrology

WORKDIR /home/ubuntu/Hydrology
RUN python setup.py install

WORKDIR /home/ubuntu/
