import sys

def write_output(dictionary,writer,delimiter):
    for key in dictionary.keys():
        for i in range(0,len(key.split('_'))):
            writer.write(key.split('_')[i]+delimiter)
        for i in range(0,len(dictionary[key])):
            writer.write(str(dictionary[key][i])+",")
        writer.write("\n")

##def test_write_output(dictionary,delimiter):
##    for key in dictionary.keys():
##        for i in range(0,len(key.split('_'))):
##            sys.stdout.write(key.split('_')[i]+delimiter)
##        for i in range(0,len(dictionary[key])):
##            sys.stdout.write(str(dictionary[key][i])+",")
##        sys.stdout.write("\n")

