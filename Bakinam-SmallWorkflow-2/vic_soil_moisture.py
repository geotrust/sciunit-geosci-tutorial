#
# Author:      Mirzah Billah
# Modified:    Bakinam Essawy
# Created:     March 11, 2013
# Copyright:   (c) Billah 2014
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#--------------------------Imported libraries-----------------------------------

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.dates import YEARLY, DateFormatter, rrulewrapper, RRuleLocator, drange, MonthLocator, WeekdayLocator
from datetime import datetime as mk
import time
import os
import pylab
from pylab import *
from matplotlib.font_manager import fontManager, FontProperties
#-------------------------------------------------------------------------------
clf()


# ploting dimension
fig = plt.figure(figsize=(5.5,1.5), dpi=None, facecolor=None, edgecolor=None, linewidth=1.0, frameon=True, subplotpars=None)
## adjusting the plot size, creating titles
fig.subplots_adjust(wspace=0.3, hspace=0.5, left=0.09, bottom=0.20, top=0.85, right=0.98)
# data for zero axis
zeros = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

# reading the csv file that was output from vic_montly_soilmoisture.py
reader = open(r"./monthlySoilMoistureEcohydro.csv")
# skips header
reader.readline()

#get list of watershed ids

# w is a set that will include all the watershed id as string (as it has a string value)
w = {}
for line in reader:
    values = line.split(",")
    ID = values[0]
    w[str(ID)] = ID


#create legends for the lines
lines = []
series_labels = []

# loop through watershedID elements

#position of subplot
s = 0


for i in w:
    label = ''
    if (i=='mc'):
        label = label + "Soil Moisture Analysis"
        s = 1


#setup figure
   # rc('text', usetex=True)
    ax1 = fig.add_subplot(1, 1, s)

# reading the csv file
    reader = open(r"monthlySoilMoistureEcohydro.csv")
    reader.readline() # skips header
# four arrays will be open to append the values of the columns in the csv file/
# as first array will include the month,
    month = []
    evap_mct= []
    evap_mcm = []
    evap_mcd = []

    high = 0;
    low = 100;
    for line in reader:
        values = line.split(",")
        watershedID = values[0]

        if str(watershedID) == i:
            m = values[1]
##            val = values[3]
            values[1] = m;
            month.append(mk.fromtimestamp(time.mktime(time.strptime(values[1],'%Y%m'))))
#appendiing the soil moisture for the three layers as percentage
            evap_mct.append(float(values[2])*100)  ## add monthly mct data
            evap_mcm.append(float(values[3])*100)  ## add monthly mcm data
            evap_mcd.append(float(values[4])*100)  ## add monthly mcd data



#ploting evap(mct)  values in the primary axis(x-axis)
    ax1evap_mct = ax1
    evap1_mct = []
    for i in evap_mct:
        if i =='-9999999':
            evap1_mct.append('NaN')
        else:
            evap1_mct.append(i)

    pevap_mct = [];
    qevap_mct = [];
    for i in range(0,len(month)):
        if(month[i].month == 12):
            pevap_mct.append(date2num(month[i]));
            qevap_mct.append(evap1_mct[i]);

        else:
            pevap_mct.append(date2num(month[i]));
            qevap_mct.append(evap1_mct[i]);


    evap2_mct = ma.masked_where(qevap_mct == 'NaN', qevap_mct)
    ax1evap_mct.plot_date(pevap_mct,qevap_mct,'s-', color='blue', markersize=2.0, linewidth = 0.5)
    ax1evap_mct.xaxis.set_major_formatter(DateFormatter("%b-%y"))
    ax1evap_mct.xaxis.set_minor_locator(MonthLocator(interval=1))
    xlim(date2num(month[0]), date2num(month[119]))
    if (s==1):setp(ax1evap_mct.get_xticklabels() , fontsize = 6)
    setp(ax1evap_mct.get_yticklabels() , fontsize = 5)
    if (s == 1): ylabel('Monthly Average Soil Moisture ($\%$)',size=5)
    ylim(0,50)

 # ploting evap(mcm)  values in the primary axis(x-axis)
    ax1evap_mcm = ax1
    evap1_mcm = []
    for i in evap_mcm:
        if i =='-9999999':
            evap1_mcm.append('NaN')
        else:
            evap1_mcm.append(i)

    pevap_mcm = [];
    qevap_mcm = [];
    for i in range(0,len(month)):
        if(month[i].month == 12):
            pevap_mcm.append(date2num(month[i]));
            qevap_mcm.append(evap1_mcm[i]);

        else:
            pevap_mcm.append(date2num(month[i]));
            qevap_mcm.append(evap1_mcm[i]);


    evap2_mcm = ma.masked_where(qevap_mcm == 'NaN', qevap_mcm)
    ax1evap_mcm.plot_date(pevap_mcm,qevap_mcm,'-D', color='red', markersize=2.0, linewidth = 1.0)
    ax1evap_mcm.xaxis.set_major_formatter(DateFormatter("%b-%y"))
    ax1evap_mcm.xaxis.set_minor_locator(MonthLocator(interval=1))
    xlim(date2num(month[0]), date2num(month[119]))
##    setp(ax1evap_mcm.get_xticklabels() , fontsize = 6)
##    setp(ax1evap_mcm.get_yticklabels() , fontsize = 5)
    ylim(0,50)

## ploting corrected evap(RS)  values in the primary axis(x-axis)
    ax1evap_mcd = ax1
    evap1_mcd = []
    for i in evap_mcd:
        if i =='#DIV/0!\n':
            evap1_mcd.append('NaN')
        else:
            evap1_mcd.append(i)

    pevap_mcd = [];
    qevap_mcd = [];
    for i in range(0,len(month)):
        if(month[i].month == 12):
            pevap_mcd.append(date2num(month[i]));
            qevap_mcd.append(evap1_mcd[i]);

        else:
            pevap_mcd.append(date2num(month[i]));
            qevap_mcd.append(evap1_mcd[i]);


    evap2_mcd = ma.masked_where(qevap_mcd == 'NaN', qevap_mcd)
    ax1evap_mcd.plot_date(pevap_mcd,qevap_mcd,'-go', color='green', markersize=2.0, linewidth = 1.0)
    ax1evap_mcd.xaxis.set_major_formatter(DateFormatter("%b-%y"))
    ax1evap_mcd.xaxis.set_minor_locator(MonthLocator(interval=1))
    xlim(date2num(month[0]), date2num(month[119]))
    ylim(0,50)

# adjusting the plot size, creating titles
    plt.title(label, fontsize = 7)


lines.append (ax1.lines)

font = FontProperties(size=4)

legend(['Soil Moisture [top]','Soil Moisture [mid]', 'Soil Moisture [deep]'],prop={'size':4.25},loc='upper right')
savefig('vicSoilMoistureEcohydro.pdf')

print 'Third python Script executed Succefully'