from setuptools import setup

setup(name="scidataspace-sample-workflow",
      version="1.0",
      description="Small Workflow",
      long_description=open("README.rst").read(),
      author="Bakinam Essawy",
      author_email="bakinam@uva.edu",
      install_requires=['numpy>=1.12.1rc1','matplotlib>=2.0.0','boto>=2.46.1']
      )
