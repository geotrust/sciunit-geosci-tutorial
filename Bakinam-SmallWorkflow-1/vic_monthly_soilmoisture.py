# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        vic_monthly_soilmoisture
# Purpose:      create monthly average soil moisture over study area
#
#
# Author:      Mirzah Billah
# Created:     March 11, 2013
# Modified:    Bakinam Essawy
# Copyright:   (c) Billah 2014
#-------------------------------------------------------------------------------



#-------------------------------------------------------------------------------
#--------------------------Imported libraries-----------------------------------

import sets
import numpy
import csv
import operator
from operator import itemgetter
#-------------------------------------------------------------------------------

#------------------- path of the input files------------------------------------
# This input file is the output from the previous python script vic_calc_mnth.py
reader = open(r"spatiotempSoilMoistureEcohydro.csv",'r')
reader.readline()
dates =[]
# Comma seeparator
for line in reader:
    values = line.split(",")
    duration = values[2]
    dates.append(duration)

Set = sets.Set(dates)
dates = list(Set)
dates.sort()

dt_vs = {}
for date in dates:
    vmct =[]
    vmcm =[]
    vmcd =[]

    reader = open(r"./spatiotempSoilMoistureEcohydro.csv",'r')
    reader.readline()


    amct = 0
    amcm = 0
    amcd = 0
# Values in columnn 2 is fo rthe duration(year and month), column 5 is for the/
#monthly top layer soil moisture, column 6 is for middle layer soil moisture/
#column 7 is for deep layer soil moisture
    for line in reader:
        vals = line.split(",")
        duration = vals[2]
        if vals[5] != 'nan':
            mct = float(vals[5])
        if vals[6] != 'nan':
            mcm = float(vals[6])
        if vals[7] != 'nan':
            mcd = float(vals[7])
#vmct, wmcm and vmcd is used to append the soil moisture for the three layers
        if duration == date:
            vmct.append(float(mct))
            vmcm.append(float(mcm))
            vmcd.append(float(mcd))

# averaging the soil moisture

    amct = sum(vmct)/len(vmct)
    amcm = sum(vmcm)/len(vmcm)
    amcd= sum(vmcd)/len(vmcd)

    vs = (amct,amcm,amcd)

    dt_vs[date]=vs
    sorted_x = sorted(dt_vs.iteritems(), key = operator.itemgetter(0))

# write the dictionary into csv files

#opening a csv file to write the output in
writer = open(r"monthlySoilMoistureEcohydro.csv", "w")

#Creating the file header

writer.write("wid,duration,SoilMoisture_T(%),SoilMoisture_M(%),SoilMoisture_D(%)\n")

# sort the data according to date
for date, vs in  sorted_x:
    writer.write("mc," + date +",%3.2f,%3.2f,%3.2f\n" % (vs[0],vs[1],vs[2]))
writer.close()

print 'Second python Script executed Succefully'

